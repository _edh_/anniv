#! /usr/bin/env python3

import argparse
import datetime
import itertools
import jinja2
import locale
import pathlib
import tomllib


class Person:
    def __init__(self, name, birth):
        self.name = name
        self.birth = birth

    def __str__(self):
        return self.name
    __repr__ = __str__

    def __lt__(self, other):
        # used for sorting Persons
        return self.birth < other.birth

    def age(self, date):
        # return written age in french at the given date. ex: "53 ans, 2 mois et 8 jours"
        ay = date.year - self.birth.year
        am = date.month - self.birth.month
        ad = date.day - self.birth.day
        if ad < 0:
            am -= 1
            ad = 31 + ad
        if am < 0:
            ay -= 1
            am = 12 + am
        age = []
        if ay == 1:
            age.append(f"{ay} an")
        elif ay > 1:
            age.append(f"{ay} ans")
        if am:
            age.append(f"{am} mois")
        if ad == 1:
            age.append(f"{ad} jour")
        elif ad > 1:
            age.append(f"{ad} jours")
        if len(age) == 3:
            age.insert(1, ", ")
        if len(age) > 1:
            age.insert(-1, " et ")
        return "".join(age)


# Given two groups of persons, returns the dates at which
# cumulative ages of one group equals k times the cumulative ages of the other
# with k in [1;5]
def compare(group1, group2):
    g1 = sorted(group1)
    alpha1 = len(g1)
    t1 = g1[-1].birth
    a1 = sum((t1 - p.birth for p in g1), start=datetime.timedelta(0))
    g2 = sorted(group2)
    alpha2 = len(g2)
    t2 = g2[-1].birth
    a2 = sum((t2 - p.birth for p in g2), start=datetime.timedelta(0))
    for k in range(1, 6):
        if k * alpha2 - alpha1 != 0:
            date = t2 + (a1 - k * a2 + (t2 - t1) * alpha1) / (k * alpha2 - alpha1)
            if date > t1 and date > t2:
                yield date, k, g1, g2
        if k != 1 and k * alpha1 - alpha2 != 0:
            date = t1 + (a2 - k * a1 + (t1 - t2) * alpha2) / (k * alpha1 - alpha2)
            if date > t1 and date > t2:
                yield date, k, g2, g1


# Returns the sorted anniversary dates of a family
def anniversaries(family):
    family = [Person(name, birth) for name, birth in family.items()]

    dates = []

    # Generate all 2-uples of persons groups inside family and compare them
    for cardinal in range(2, len(family) + 1):
        for card1 in range(1, cardinal // 2 + 1):
            for persons in itertools.combinations(family, cardinal):
                cache = set()
                for group1 in itertools.combinations(persons, card1):
                    if frozenset(group1) not in cache:
                        group2 = tuple(frozenset(persons) - frozenset(group1))
                        cache.add(frozenset(group2))
                        for date, k, g1, g2 in compare(group1, group2):
                            dates.append((date, (k, g1, g2)))

    # Complete the list of dates with birthdays in 1000-days
    for person in family:
        for k in range(1, 37):
            date = person.birth + datetime.timedelta(k * 1000)
            dates.append((date, (k, person)))

    return sorted(dates, key=lambda i: i[0])


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--directory', help="directory where to write HTML outputs", type=pathlib.Path)
parser.add_argument('families', help="Families description (TOML) file", type=argparse.FileType('rb'))
args = parser.parse_args()

try:
    families = tomllib.load(args.families)
    directory = (args.directory or pathlib.Path(__file__).parent).absolute()
    directory.mkdir(parents=True, exist_ok=True)
except Exception as error:
    parser.error(error)

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
template = env.get_template("anniv.html")
for familyname, family in families.items():
    open(directory / f'{familyname}.html', 'w').write(
        template.render(familyname=familyname, family=family, dates=anniversaries(family)))
template = env.get_template("index.html")
open(directory / 'index.html', 'w').write(
    template.render(familynames=families.keys()))
